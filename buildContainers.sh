#!/bin/sh

RIR_VERSION="dba88e9bc417325a29c91acb088df7fe8109ca39"

for experiment in `ls experiments`; do
  cd experiments/$experiment
  docker build --build-arg RIR_VERSION=$RIR_VERSION -t registry.gitlab.com/rirvm/rir_experiments/$experiment:$RIR_VERSION-$CI_COMMIT_SHA .
  docker push registry.gitlab.com/rirvm/rir_experiments/$experiment:$RIR_VERSION-$CI_COMMIT_SHA
  cd ../..
done
