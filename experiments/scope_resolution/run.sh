#!/bin/sh
cd /opt

sed "s/PIR_PASS_BLACKLIST_PARAMETER/''/" rebench.conf.template > rebench.conf
rebench rebench.conf -df baseline.data > /dev/null
/opt/rir-rebench-scripts/dump.rb baseline.data  | sed 's/^/baseline, /'
echo "******"

sed "s/PIR_PASS_BLACKLIST_PARAMETER/'ForceDominance'/" rebench.conf.template > rebench.conf
rebench rebench.conf -df noForceDominance.data > /dev/null
/opt/rir-rebench-scripts/dump.rb noForceDominance.data | sed 's/^/no promise inlining, /'
echo "******"

sed "s/PIR_PASS_BLACKLIST_PARAMETER/'ScopeResolution'/" rebench.conf.template > rebench.conf
rebench rebench.conf -df noScopeResolution.data > /dev/null
/opt/rir-rebench-scripts/dump.rb noScopeResolution.data | sed 's/^/no scope resolution, /'
echo "******"

sed "s/PIR_PASS_BLACKLIST_PARAMETER/'.*'/" rebench.conf.template > rebench.conf
rebench rebench.conf -df none.data > /dev/null
/opt/rir-rebench-scripts/dump.rb none.data | sed 's/^/none, /'
