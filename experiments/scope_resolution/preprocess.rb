#!/usr/bin/env ruby

f = File.read("scope_resolution.csv")
r = f.split("******\n").map{|p| p.split("\n").map{|l| l.split(",")}}

r.first.size.times do |i|
  baseline = r[0][i]
  raise "not bl" unless baseline[0] == "baseline"
  pos = 0
  r.each do |p|
    next if p[i] == baseline
    pos += 1
    raise "wrong bm #{p[i][1]}" unless p[i][1] == baseline[1]
    puts "#{p[i][0]}, #{p[i][1]}, #{Float(p[i][2])/Float(baseline[2])},#{pos}\n"
  end
end
