#!/bin/sh

cd /opt
./run.sh > scope_resolution.csv
ruby preprocess.rb > scope_resolution_pre.csv
echo "*** raw results"
cat scope_resolution_pre.csv
Rscript plot.R > /dev/null
echo "*** pdf"
cat scope_resolution.pdf
