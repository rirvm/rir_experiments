#!/bin/bash
set -e

cd /opt/rir/build/debugopt

run_experiment () {
  EXPERIMENT_NAME="$1"
  shift
  EXPERIMENT="$@"

  ENABLE_EVENT_COUNTERS=1 bin/R $EXPERIMENT &> /dev/null
  sed  "s/^/$EXPERIMENT_NAME, /" rir_statistics.csv >> results.csv
  rm rir_statistics.csv

  ENABLE_EVENT_COUNTERS=1 PIR_ENABLE=off bin/R $EXPERIMENT &> /dev/null
  echo "$EXPERIMENT_NAME, baseline $(grep 'env allocated' rir_statistics.csv)" >> results.csv
  rm rir_statistics.csv
}

run_experiment "check_code_usage_in_package" "-f ../../rir/tests/pir_regression_check_code.R"
run_experiment "demos.R" "-f /opt/demos.R"
run_experiment "tcltk examples" "-f /opt/tcltk-Ex.R"
run_experiment "stats examples" "-f /opt/stats-Ex.R"
run_experiment "utils examples" "-f /opt/utils-Ex.R"
run_experiment "pidigits" "-f /opt/pidigits.R"
run_experiment "mandelbrot.R" "-f /opt/mandelbrot.R"

echo "===== Raw data"
cat results.csv

echo "===== Environments omited"
Rscript /opt/results.R
